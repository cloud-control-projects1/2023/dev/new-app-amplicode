module "newamplicode_db" {
  source = "./db"

  name                                  = var.newamplicode_db_name
  engine                                = var.newamplicode_db_engine
  engine_version                        = var.newamplicode_db_engine_version
  instance_class                        = var.newamplicode_db_instance_class
  storage                               = var.newamplicode_db_storage
  user                                  = var.newamplicode_db_user
  password                              = var.newamplicode_db_password
  random_password                       = var.newamplicode_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.newamplicode_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
}
