output "private_subnets" {
  value = aws_subnet.private
}

output "public_subnets" {
  value = aws_subnet.public
}

output "private_subnets_es" {
  value = aws_subnet.private_es
}

output "db_subnets" {
  value = aws_subnet.db
}

output "db_subnet_group_name" {
  value = aws_db_subnet_group.this.name
}

output "id" {
  value = aws_vpc.main.id
}